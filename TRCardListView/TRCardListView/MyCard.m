//
//  MyCard.m
//  TRCardListView
//
//  Created by cry on 2017/6/13.
//  Copyright © 2017年 eGova. All rights reserved.
//

#import "MyCard.h"

@implementation MyCard

- (instancetype)initWithFrame:(CGRect)frame{
    
    if (self = [super initWithFrame:frame]) {
        self.backgroundColor = [UIColor colorWithRed:0.9 green:0.9 blue:0.9 alpha:1];
        self.layer.borderColor = [UIColor whiteColor].CGColor;
        self.layer.borderWidth = 1;
        /**************添加子视图到view上****************/
        _label = [[UILabel alloc] initWithFrame:CGRectMake(0, 1, frame.size.width, 63)];
        _label.backgroundColor = [UIColor lightGrayColor];
        _label.textColor = [UIColor whiteColor];
        _label.textAlignment = 1;
        [self addSubview:_label];
    }
    return self;
}

@end
