//
//  ViewController.m
//  TRCardListView
//
//  Created by cry on 2017/6/13.
//  Copyright © 2017年 eGova. All rights reserved.
//

#import "ViewController.h"
#import "TRCardListView.h"
#import "MyCard.h"

@interface ViewController ()<TRCardListViewDataSource, TRCardListViewDelegate>

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.
    // UITableView
    
    TRCardListView *cardListView = [[TRCardListView alloc] initWithFrame:self.view.bounds];
    cardListView.trDelegate = self;
    cardListView.trDataSource = self;
    [cardListView registerForReuseWithClass:[MyCard class]];
    [self.view addSubview:cardListView];
}

- (NSInteger)tr_numberOfCardsInCardListView:(TRCardListView *)cardListView{
    return 24;
}

- (UIView *)tr_cardListView:(TRCardListView *)cardListView cardAtIndex:(NSInteger)index{
    MyCard *card = (MyCard *)[cardListView dequeueReusableCardAtIndex:index];
    card.label.text = [NSString stringWithFormat:@"第%ld页", index];
    return card;
}

- (void)tr_cardListView:(TRCardListView *)cardListView didSelectCardAtIndex:(NSInteger)index{
    NSLog(@"%ld", index);
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


@end
